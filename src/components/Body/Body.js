import React from 'react';
import {Switch, Route} from "react-router-dom";

import "./Body.scss";
import MainPage from "../Pages/MainPage/MainPage";
import Cart from "../Pages/Cart/Cart";
import Favourites from "../Pages/Favourites/Favourites";

const Body = () => {


     return (
        <div className="page">
            <Switch>
                <Route path="/" exact>
                    <MainPage/>
                </Route>
                <Route path="/cart" exact>
                    <Cart/>
                </Route>
                <Route path="/favourite" exact>
                    <Favourites/>
                </Route>
            </Switch>
        </div>
    );
}


export default Body;