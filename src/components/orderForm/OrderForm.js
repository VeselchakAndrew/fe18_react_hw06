import React from "react";
import {Formik, Form, Field, ErrorMessage} from "formik";
import * as Yup from "yup";

import ErrorMsg from "./ErrorMsg/ErrorMsg";
import "./OrderForm.scss"
import {useDispatch, useSelector} from "react-redux";
import {saveOrderData} from "../../redux/actions/formAction";
import {removeCartInfoFromLS} from "../../redux/actions/itemsAction";

const OrderForm = () => {

    const dispatch = useDispatch();

    const goodInCart = useSelector(state => state.items.goodsInCart);

    const initialValues = {
        name: "",
        lastName: "",
        age: "",
        address: "",
        telephone: ""
    }

    const onSubmit = (values, helpers) => {
        console.log(values);
        console.log(goodInCart);
        helpers.setSubmitting(false);
        dispatch(saveOrderData(values));
        dispatch(removeCartInfoFromLS());
    }

    const validationSchema = Yup.object().shape({
        name: Yup.string().required("Введите имя"),
        lastName: Yup.string().required("Введите фамилию"),
        age: Yup.number().min(10, "Вы слишком юны для заказа").max(100, "Да Вы долгожитель :)").typeError("Должно быть число").required("Введите возраст"),
        address: Yup.string().required("Введите адрес доставки"),
        telephone: Yup.number().typeError("Должно быть число").required("Введите номер телефона"),
    })

    return (
        <div className="order_form">
            <h3 className="form_title">Форма заказа</h3>
            <Formik
                initialValues={initialValues}
                onSubmit={onSubmit}
                validationSchema={validationSchema}>
                {(formikProps) => {
                    return (
                        <Form>
                            <div className="form-control">
                                <div className="label">
                                    <label htmlFor="name">Имя</label>
                                </div>
                                <div>
                                    <Field type="text"
                                           id="name"
                                           name="name"
                                           placeholder="Ваше имя"
                                           className="form_field"/>
                                    <ErrorMessage name="name" component={ErrorMsg}/>
                                </div>
                            </div>

                            <div className="form-control">
                                <div className="label">
                                    <label htmlFor="lastName">Фамилия</label>
                                </div>
                                <div>
                                    <Field id="lastName"
                                           name="lastName"
                                           placeholder="Ваша фамилия"
                                           className="form_field"/>
                                    <ErrorMessage name="lastName" component={ErrorMsg}/>
                                </div>
                            </div>

                            <div className="form-control">
                                <div className="label">
                                    <label htmlFor="age">Возраст</label>
                                </div>
                                <div>
                                    <Field id="age"
                                           name="age"
                                           placeholder="Ваш возраст"
                                           className="form_field"/>
                                    <ErrorMessage name="age" component={ErrorMsg}/>
                                </div>
                            </div>

                            <div className="form-control">
                                <div className="label">
                                    <label htmlFor="address">Адрес</label>
                                </div>
                                <div>
                                    <Field as="textarea"
                                           id="address"
                                           name="address"
                                           placeholder="Адрес доставки"
                                           className="form_field"/>
                                    <ErrorMessage name="address" component={ErrorMsg}/>
                                </div>
                            </div>

                            <div className="form-control">
                                <div className="label">
                                    <label htmlFor="name">Номер телефона</label>
                                </div>
                                <div>
                                    <Field id="telephone"
                                           name="telephone"
                                           placeholder="Ваш номер телефона"
                                           className="form_field"/>
                                    <ErrorMessage name="telephone" component={ErrorMsg}/>
                                </div>
                            </div>
                            <button type="submit" disabled={formikProps.isSubmitting} className="order_button">Checkout</button>
                        </Form>
                    )
                }}

            </Formik>
        </div>
    );
};

export default OrderForm;