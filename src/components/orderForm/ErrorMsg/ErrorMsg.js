import React from "react";

const ErrorMsg = ({children}) => {
    return (
        <div className="error">
            {children}
        </div>
    );
};

export default ErrorMsg;