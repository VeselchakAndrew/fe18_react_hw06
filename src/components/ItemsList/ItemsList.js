import React, {useState} from "react";
import PropTypes from "prop-types";

import Card from "../Card/Card";
import Button from "../Button/Button";
import CustomButton from "../CustomButton/CustomButton";
import ModalWindow from "../ModalWindow/ModalWindow";
import {useDispatch, useSelector} from "react-redux";
import {addToCart, removeFromCart, setFavorite, updateData} from "../../redux/actions/itemsAction";
import {
    showAddToCartModalWindow,
    showConfirmDeletionModalWindow,
    closeModalWindow
} from "../../redux/actions/modalAction";


const ItemsList = (props) => {
    const {goodsData, isBtnExist, isFavouriteExist, isDeleteExist} = props;

    const [itemIdToRemove, setItemIdToRemove] = useState(null);
    const [itemIdToAdd, setItemIdToAdd] = useState(null);

    const dispatch = useDispatch();
    const goodsInCartID = useSelector(state => state.items.goodsInCartID);
    const goodsInFavouriteID = useSelector(state => state.items.goodsInFavouriteID);
    const modalsStatus = useSelector(state => ({
        showAddToCart: state.modals.showAddToCartModalWindow,
        showConfirmDelete: state.modals.showConfirmDeleteModalWindow
    }))

    const showAddToCartModal = () => {
        dispatch(showAddToCartModalWindow());

        document.body.classList.add("modal-open");
    }

    const showRemoveFromCartModal = () => {
        dispatch(showConfirmDeletionModalWindow());

        document.body.classList.add("modal-open");
    }

    const closeModal = () => {
        dispatch(closeModalWindow());

        document.body.classList.remove("modal-open");
    }

    const removeBtnHandler = (id) => {
        setItemIdToRemove(id);
        showRemoveFromCartModal();
    }

    const removeFromCartHandler = (id) => {
        dispatch(removeFromCart(goodsInCartID, id))
        closeModal();
        setItemIdToRemove(null);
        dispatch(updateData(true));

    }

    const handleAddClick = (id) => {
        showAddToCartModal();
        setItemIdToAdd(id);
        dispatch(updateData(true));
    }

    const handleOKClick = () => {
        dispatch(addToCart(goodsInCartID, itemIdToAdd));
        closeModal();
    }

    const favouriteBtnHandler = (id) => {
        dispatch(setFavorite(goodsInFavouriteID, id));
        dispatch(updateData(true));
    }

    const cards = goodsData.map(good => (
            <div key={good.vendorCode}>
                <Card good={good}>
                    <>
                        {isBtnExist &&
                        <Button btnClass="add_btn" btnAction={() => handleAddClick(good.vendorCode)}>
                            {goodsInCartID.includes(good.vendorCode) ? "Already in cart" : "Add to cart"}
                        </Button>}

                        {isFavouriteExist && <CustomButton
                            customBtnClass={goodsInFavouriteID.includes(good.vendorCode)
                                ? "favourite favourite_is_checked"
                                : "favourite"}
                            btnAction={() => favouriteBtnHandler(good.vendorCode)}/>}

                        {isDeleteExist && <CustomButton
                            customBtnClass='delete_btn'
                            btnAction={() => removeBtnHandler(good.vendorCode)}/>}
                    </>
                </Card>

            </div>
        )
    )


    return (
        <>
            {cards}

            {modalsStatus.showAddToCart &&
            !goodsInCartID.includes(itemIdToAdd) &&
            <ModalWindow closeWindow={closeModal}
                         info={"Добавить товар в корзину?"}
                         children={
                             <>
                                 <Button btnClass="add_btn" btnAction={handleOKClick}>OK</Button>
                                 <Button btnClass="add_btn"
                                         btnAction={closeModal}>Cancel</Button>
                             </>}
            />}

            {modalsStatus.showAddToCart &&
            goodsInCartID.includes(itemIdToAdd) &&
            <ModalWindow closeWindow={closeModal}
                         info={"Товар уже в корзине!"}
                         children={
                             <Button btnClass="add_btn" btnAction={closeModal}>Cancel</Button>
                         }

            />}

            {modalsStatus.showConfirmDelete &&
            <ModalWindow closeWindow={closeModal}
                         info={"Вы действительно хотите удалить товар из корзины?"}
                         children={
                             <>
                                 <Button
                                     btnAction={() => removeFromCartHandler(itemIdToRemove)}>OK</Button>
                                 <Button btnAction={closeModal}>Cancel</Button>
                             </>
                         }
            />}
        </>
    )
}

export default ItemsList;

ItemsList.propTypes = {
    goodsData: PropTypes.array,
    isBtnExist: PropTypes.bool,
    isFavouriteExist: PropTypes.bool,
    isDeleteExist: PropTypes.bool,

}

ItemsList.defaultProps = {
    goodsData: [],
    isBtnExist: false,
    isFavouriteExist: false,
    isDeleteExist: false,
}
