import React from "react";
import PropTypes from "prop-types";
import "./ModalWindow.scss";

const ModalWindow = (props) => {
    const {closeWindow, info, children} = props;

    return (
        <div onClick={closeWindow} className="modalbg">
            <div className="modal" onClick={preventCloseModal}>
                <div className="modal_header">
                    <div className="cross" onClick={closeWindow}/>
                </div>
                <div className="main_content">
                    <div>{info}</div>
                </div>
                {children}
            </div>

        </div>
    );

};
const preventCloseModal = (e) => {
    e.stopPropagation();
};

ModalWindow.propTypes = {
    closeWindow: PropTypes.func.isRequired,
    info: PropTypes.string,
    children: PropTypes.element.isRequired
};

ModalWindow.defaultProps = {
    info: "Modal Window Text"
};


export default ModalWindow;