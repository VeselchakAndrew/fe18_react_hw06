import React from "react";
import {render, fireEvent} from "@testing-library/react";
import ModalWindow from "./ModalWindow";

const mockFunction = jest.fn();
const MockComponent = () => (<div>Hello!</div>);

describe("Testing custom button", () => {
    test("Modal window smoke test", () => {
        render(<ModalWindow closeWindow={mockFunction}>
            <MockComponent/>
        </ModalWindow>);
    });

    test("Modal window default info", () => {
        const {getByText} = render(<ModalWindow closeWindow={mockFunction}>
            <MockComponent/>
        </ModalWindow>);
        getByText("Modal Window Text");
    });

    test("Modal window default info", () => {
        const propsInfo = "Text";
        const {getByText} = render(<ModalWindow closeWindow={mockFunction} info={propsInfo}>
            <MockComponent/>
        </ModalWindow>);
        getByText(propsInfo);
    });

    test("Testing modal function", () => {
        render(<ModalWindow closeWindow={mockFunction}>
            <MockComponent/>
        </ModalWindow>);
        const closeBtnElement = document.querySelector(".cross");
        fireEvent.click(closeBtnElement);
        expect(mockFunction).toHaveBeenCalled();
        expect(mockFunction).toHaveBeenCalledTimes(1);
    });
});