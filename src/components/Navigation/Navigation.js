import React from 'react';
import {NavLink} from "react-router-dom";
import "./Navigation.scss";
const Navigation = () => {
    return (
        <ul>
            <NavLink exact to="/" className="navigation" activeClassName="navigation_active">Главная</NavLink>
            <NavLink exact to="/favourite" className="navigation" activeClassName="navigation_active">Избранное</NavLink>
            <NavLink exact to="/cart" className="navigation" activeClassName="navigation_active">Корзина</NavLink>
        </ul>
    );
};

export default Navigation;