import React, {useEffect} from 'react';
import ItemsList from "../../ItemsList/ItemsList";
import OrderForm from "../../orderForm/OrderForm";

import {useDispatch, useSelector} from "react-redux";
import {getDataIDFromLS, matchData, updateData} from "../../../redux/actions/itemsAction";

const Cart = () => {
    const dispatch = useDispatch();
    const goodsInCart = useSelector(state => state.items.goodsInCart);
    const isUpdated = useSelector(state => state.items.isUpdated);

    //Получаем начальные данные
    useEffect(() => {
        dispatch(matchData("Cart", "CART"));
        dispatch(getDataIDFromLS("Cart"));
        dispatch(updateData(false))
    }, [isUpdated]);


    return (
        <>
            {goodsInCart.length === 0 ?
                <div className="empty_page">
                    <h2>Корзина пустая</h2>
                </div> : (
                    <div className="cart_container">
                        <div className="cart">
                            <ItemsList goodsData={goodsInCart}
                                       isDeleteExist={true}
                            />
                        </div>
                        <OrderForm/>
                    </div>
                )}

        </>
    );
};

export default Cart;