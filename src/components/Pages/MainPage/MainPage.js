import React, {useEffect} from 'react';
import ItemsList from "../../ItemsList/ItemsList";
import {useDispatch, useSelector} from "react-redux";
import {getDataIDFromLS, getGoods, updateData} from "../../../redux/actions/itemsAction";


const MainPage = () => {

    const dispatch = useDispatch();

    const goods = useSelector(state => state.items.goods);
    const isLoading = useSelector(state => state.items.isLoading);
    const isUpdated = useSelector(state => state.items.isUdated)

    useEffect(() => {
        dispatch(getGoods());
        dispatch(getDataIDFromLS("Cart"));
        dispatch(getDataIDFromLS("Favourite"));
        dispatch(updateData(false))
    }, [isUpdated])

    return (
        <>
            {isLoading && <h2 className="loader">Loading...</h2>}
            <ItemsList goodsData={goods}
                       isBtnExist={true}
                       isFavouriteExist={true}
            />
        </>
    );
};

export default MainPage;