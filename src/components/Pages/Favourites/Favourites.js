import React, {useEffect} from 'react';
import ItemsList from "../../ItemsList/ItemsList";
import {getDataIDFromLS, matchData, updateData} from "../../../redux/actions/itemsAction";
import {useDispatch, useSelector} from "react-redux";

const Favourites = () => {
    const dispatch = useDispatch();
    const goodsInFavourite = useSelector(state => state.items.goodsInFavourite);
    const isUpdated = useSelector(state => state.items.isUpdated);

    //Получаем начальные данные
    useEffect(() => {
        dispatch(matchData("Favourite", "FAVOURITE"));
        dispatch(getDataIDFromLS("Cart"));
        dispatch(getDataIDFromLS("Favourite"));
        dispatch(updateData(false))

    }, [isUpdated]);

    return (
        <>
            {goodsInFavourite.length === 0 &&
            <div className="empty_page">
                <h2>В избранном ничего нет :(</h2>
            </div>}
            <ItemsList goodsData={goodsInFavourite}
                       isBtnExist={true}
                       isFavouriteExist={true}
            />
        </>
    );
};

export default Favourites;