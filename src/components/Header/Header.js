import React, {Component} from 'react';
import Navigation from "../Navigation/Navigation";
import "./Header.scss"

class Header extends Component {
    render() {
        return (
            <div className="header">
                <div className="logo">
                    <img src="./logo/w4k_logo.png" alt="Logo"/>
                </div>
                <div className="header__title_container">
                    <h1 className="header__main_title">Warhammer 4k - Space Marines</h1>
                    <Navigation />
                </div>
                <div className="header__cart">

                </div>
            </div>
        );
    }
}

export default Header;