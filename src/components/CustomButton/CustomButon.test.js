import React from "react";
import {render, fireEvent} from "@testing-library/react";
import CustomButton from "./CustomButton";

const btnAction = jest.fn();

describe("Testing custom button", () => {
    test("Custom button smoke test", () => {
        render(<CustomButton/>);
    });

    test("Testing button props style", () => {
        const customClass = "customBtnClassName";
        render(<CustomButton customBtnClass={customClass}/>);
        const btnElement = document.querySelector("button");
        expect(btnElement).toHaveClass(customClass);
    });

    test("Testing button props function", () => {
        render(<CustomButton btnAction={btnAction}/>);
        const btnElement = document.querySelector("button");
        fireEvent.click(btnElement);
        expect(btnAction).toHaveBeenCalled();
        expect(btnAction).toHaveBeenCalledTimes(1);
    });
});

