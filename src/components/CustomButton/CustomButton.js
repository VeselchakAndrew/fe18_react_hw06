import React from "react";
import "./CustomButton.scss";

const CustomButton = (props) => {
    const {customBtnClass, btnAction} = props;
    return (
        <button className={customBtnClass} onClick={btnAction}/>
    );
};

export default CustomButton;