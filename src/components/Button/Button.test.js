import Button from "./Button";
import React from "react";
import {render, fireEvent} from "@testing-library/react";

const func = jest.fn();

describe("Testing button component", () => {
    test("Button smoke test", () => {
        render(<Button btnAction={func}/>);
    });

    test("Testing default children value", () => {
        const {getByText} = render(<Button btnAction={func}/>);
        getByText("BUTTON");
    });

    test("Testing default style", () => {
        render(<Button btnAction={func}/>);
        const btnElement = document.querySelector("button");
        expect(btnElement).toHaveClass("add_btn");
    });

    test("Testing props children", () => {
        const title = "ADD";
        const {getByText} = render(<Button children={title} btnAction={func}/>);
        getByText(title);
    });

    test("Testing props style", () => {
        const btnStyle = ".btn_className";
        render(<Button btnClass={btnStyle} btnAction={func}/>);
        const btnElement = document.querySelector("button");
        expect(btnElement).toHaveClass(btnStyle);
    });

    test("Testing props function", () => {
        render(<Button btnAction={func}/>);
        const btnElement = document.querySelector("button");
        fireEvent.click(btnElement);
        expect(func).toHaveBeenCalled();
        expect(func).toHaveBeenCalledTimes(1);
    });
});