import React from 'react';
import PropTypes from "prop-types";
import "./Button.scss"

const Button = (props) => {

        const {children, btnClass, btnAction} = props;
        return (
            <button className={btnClass} onClick={btnAction}>{children}</button>
        );

}

Button.propTypes = {
    children: PropTypes.string,
    btnClass: PropTypes.string,
    btnAction: PropTypes.func.isRequired
}

Button.defaultProps = {
    children: "BUTTON",
    btnClass: "add_btn"
}

export default Button;