export const selectGoods = currentStore => currentStore.goods;
export const selectGoodsInCartID = currentStore => currentStore.goodsInCartID;
export const selectGoodsInFavouriteID = currentStore => currentStore.goodsInCartID;
export const selectGoodsInCart = currentStore => currentStore.goodsInCart;
export const selectGoodsInFavourite = currentStore => currentStore.goodsInFavourite;
export const selectIsLoading = currentStore => currentStore.isLoading;
