import "./itemsReducer";
import itemsReducer from "./itemsReducer";
import {
    GET_GOODS_FROM_JSON,
    GET_GOODS_ID_IN_CART,
    GET_GOODS_ID_IN_FAVOURITE,
    GET_GOODS_IN_CART,
    GET_GOODS_IN_FAVOURITE,
    IS_LOADING,
    IS_UPDATED,
    REMOVE_GOODS_FROM_CART,
    SET_GOODS_IN_FAVOURITE
} from "../itemsActionTypes";
import initialState from "./itemsReducer";


describe("Item Reducer testing", () => {
    test("Item Reducer default case", () => {

        const action = {
            type: "UNDEFINED_ACTION",
        };
        const expectedValue = itemsReducer(initialState, action);
        expect(expectedValue).toEqual(initialState);
    });

    test("Fetch goods", () => {

        const action = {
            type: GET_GOODS_FROM_JSON,
            payload: [
                {id: 1, name: "Name1"},
                {id: 2, name: "Name2"}
            ]
        };
        const expectedValue = itemsReducer(initialState, action);
        expect(expectedValue.goods).toEqual(action.payload);
    });
    test("Get goods ID in Cart", () => {

        const action = {
            type: GET_GOODS_ID_IN_CART,
            payload: [1, 2, 3, 4]
        };
        const expectedValue = itemsReducer(initialState, action);
        expect(expectedValue.goodsInCartID).toEqual(action.payload);
    });
    test("Get goods ID in Favourite", () => {

        const action = {
            type: GET_GOODS_ID_IN_FAVOURITE,
            payload: [1, 2, 3, 4]
        };
        const expectedValue = itemsReducer(initialState, action);
        expect(expectedValue.goodsInFavouriteID).toEqual(action.payload);
    });
    test("Get goods in Cart", () => {

        const action = {
            type: GET_GOODS_IN_CART,
            payload: [
                {id: 1, name: "Name1"},
                {id: 2, name: "Name2"}
            ]
        };
        const expectedValue = itemsReducer(initialState, action);
        expect(expectedValue.goodsInCart).toEqual(action.payload);
    });
    test("Get goods in Favourite", () => {

        const action = {
            type: GET_GOODS_IN_FAVOURITE,
            payload: [
                {id: 1, name: "Name1"},
                {id: 2, name: "Name2"}
            ]
        };
        const expectedValue = itemsReducer(initialState, action);
        expect(expectedValue.goodsInFavourite).toEqual(action.payload);
    });
    test("Remove good from Cart", () => {

        const prevState = {
            goodsInCartID: [1, 2, 3, 4]
        };

        const action = {
            type: REMOVE_GOODS_FROM_CART,
            payload: [1, 2, 4]
        };
        const expectedValue = itemsReducer(prevState, action);
        expect(expectedValue.goodsInCartID).toEqual(action.payload);
    });
    test("Remove good from Favourite", () => {

        const prevState = {
            goodsInFavouriteID: [1, 2, 3, 4]
        };

        const action = {
            type: SET_GOODS_IN_FAVOURITE,
            payload: [1, 2, 4]
        };
        const expectedValue = itemsReducer(prevState, action);
        expect(expectedValue.goodsInFavouriteID).toEqual(action.payload);
    });
    test("Add good to Favourite", () => {

        const prevState = {
            goodsInFavouriteID: [1, 2, 4]
        };

        const action = {
            type: SET_GOODS_IN_FAVOURITE,
            payload: [1, 2, 3, 4]
        };
        const expectedValue = itemsReducer(prevState, action);
        expect(expectedValue.goodsInFavouriteID).toEqual(action.payload);
    });
    test("Set is updated", () => {

        const action = {
            type: IS_UPDATED,
            payload: false
        };
        const expectedValue = itemsReducer(initialState, action);
        expect(expectedValue.isUpdated).toEqual(action.payload);
    });
    test("Set is loading", () => {

        const action = {
            type: IS_LOADING,
            payload: true
        };
        const expectedValue = itemsReducer(initialState, action);
        expect(expectedValue.isLoading).toEqual(action.payload);
    });

});