import modalReducer from "./modalReducer";
import initialState from "./modalReducer";
import {
    CLOSE_MODAL_WINDOW,
    SHOW_ADD_TO_CART_MODAL_WINDOW,
    SHOW_CONFIRM_DELETE_MODAL_WINDOW
} from "../modalsActionTypes";

describe("Modal Reducer testing", () => {
    test("Modal Reducer default case", () => {

        const action = {
            type: "UNDEFINED_ACTION",
        };

        const expectedValue = modalReducer(initialState, action);
        expect(expectedValue).toEqual(initialState);
    });
    test("Show 'Add to cart' modal window", () => {
        const action = {
            type: SHOW_ADD_TO_CART_MODAL_WINDOW,
            payload: true
        };

        const expectedValue = modalReducer(initialState, action);
        expect(expectedValue.showAddToCartModalWindow).toBeTruthy();
    });
    test("Show 'Confirm delete' modal window", () => {
        const action = {
            type: SHOW_CONFIRM_DELETE_MODAL_WINDOW,
            payload: true
        };

        const expectedValue = modalReducer(initialState, action);
        expect(expectedValue.showConfirmDeleteModalWindow).toBeTruthy();
    });
    test("Close modal windows", () => {
        const action = {
            type: CLOSE_MODAL_WINDOW,
        };
        const expectedValue = modalReducer(initialState, action);
        expect(expectedValue.showAddToCartModalWindow).toBeFalsy();
        expect(expectedValue.showConfirmDeleteModalWindow).toBeFalsy();
    });
});