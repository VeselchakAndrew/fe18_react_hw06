import {
    SHOW_ADD_TO_CART_MODAL_WINDOW,
    SHOW_CONFIRM_DELETE_MODAL_WINDOW,
    CLOSE_MODAL_WINDOW
} from "../modalsActionTypes";

const initialState = {
    showAddToCartModalWindow: false,
    showConfirmDeleteModalWindow: false
}

const modalReducer = (state = initialState, action) => {
    switch (action.type) {
        case SHOW_ADD_TO_CART_MODAL_WINDOW:
            return {
                ...state, showAddToCartModalWindow: true
            }

        case SHOW_CONFIRM_DELETE_MODAL_WINDOW:
            return {
                ...state, showConfirmDeleteModalWindow: true
            }

        case CLOSE_MODAL_WINDOW:
            return {
                showConfirmDeleteModalWindow: false,
                showAddToCartModalWindow: false
            }

        default:
            return state;
    }
}

export default modalReducer;