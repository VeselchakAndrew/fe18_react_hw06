import initialState from "./formReducer";
import formReducer from "./formReducer";
import {SAVE_ORDER_INFO_FROM_FORM} from "../formActionTypes";

describe("Testing formAction", () => {
    test("Form reducer testing", () => {
        const action = {
            type: SAVE_ORDER_INFO_FROM_FORM,
            payload: {
                name: "Name",
                lastName: "LastName",
                address: "Address",
                age: 35,
                telephone: 100000000000
            }
        };
        const expectedValue = formReducer(initialState, action);
        expect(expectedValue).toEqual(action.payload);
    });
});