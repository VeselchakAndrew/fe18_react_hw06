import {SAVE_ORDER_INFO_FROM_FORM} from "../formActionTypes";

export const initialState = {
    name: "",
    lastName: "",
    age: "",
    address: "",
    telephone: ""
}

export const formReducer = (state = initialState, action) => {
    switch (action.type) {
        case SAVE_ORDER_INFO_FROM_FORM:
            return {
                name: action.payload.name,
                lastName: action.payload.lastName,
                address: action.payload.address,
                age: action.payload.age,
                telephone: action.payload.telephone
            }

        default:
            return state;
    }
}
export default formReducer;